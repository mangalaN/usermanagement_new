import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TilesConfigComponent } from './tiles-config.component';

describe('TilesConfigComponent', () => {
  let component: TilesConfigComponent;
  let fixture: ComponentFixture<TilesConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TilesConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TilesConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
