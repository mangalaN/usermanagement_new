import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { iconData, colorPalette, simpleIconData } from '../../shared/data/icon-data.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { ChartCongigService } from '../../shared/data/chart-congig.service';

@Component({
  selector: 'app-tiles-config',
  templateUrl: './tiles-config.component.html',
  styleUrls: ['./tiles-config.component.scss']
})
export class TilesConfigComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  icons;
  selectedIcon = '';
  columns;
  newsletterColumns;
  colorPalettes;
  regularForm: FormGroup;
  memLoyVal = false;
  valueName;
  simpleIcon;
  tilesCount;
  newsletter = false;
  newsletterSelectedColumn = '';
  tiles;
  isCollapsed = true;
  tileEdit = 0;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(private snotifyService: SnotifyService, public chartService: ChartCongigService) {
    this.icons = iconData;
    this.colorPalettes = colorPalette;
    this.simpleIcon = simpleIconData;
    //GET TILES Count
    // this.chartService.getTilesCount(1, new Date().getTime()).then(data => {
    //   if(data['status'] == 'success'){
    //     this.tilesCount = data['count'];
    //   }
    // });
  }

  ngOnInit() {
    this.regularForm = new FormGroup({
      'name': new FormControl(null, null),
      'value': new FormControl(null, null),
      'columns': new FormControl(null, null),
      'color': new FormControl(null, null),
      'newscolumns': new FormControl(null, null)
    }, {updateOn: 'blur'});
    this.chartService.getTiles(1, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.tiles = data['tiles'];
        this.tilesCount = data['tiles'].length;
      }
      else{
        this.tiles = [];
      }
    });
  }

  editTile(id){
    this.tileEdit = id;
    this.isCollapsed = false;
    this.chartService.getTilesById(id, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.regularForm.controls['name'].setValue(data['tiles'][0]['name']);
        this.selectedIcon = data['tiles'][0]['icon'];
        this.regularForm.controls['value'].setValue(data['tiles'][0]['tableName']);
        this.regularForm.controls['columns'].setValue(data['tiles'][0]['columnName']);
        this.regularForm.controls['color'].setValue(data['tiles'][0]['color']);
        if(data['tiles'][0]['tableName'] == 'users'){
          this.chartService.getColumns(data['tiles'][0]['tableName']).then(data => {
            if(data['status'] == 'success'){
              this.columns = data['columns'];
              //console.log(this.columns);
            }
          });
          this.memLoyVal = false;
          this.newsletter = false;
          this.newsletterSelectedColumn = '';
          this.regularForm.controls['newscolumns'].setValue('');
          this.regularForm.controls['columns'].setValue(data['tiles'][0]['columnName']);
        }
        else{
          if(data['tiles'][0]['tableName'] == 'newsletter'){
            this.newsletter = true;
          }
          else{
            this.newsletter = false;
            this.newsletterSelectedColumn = '';
            this.regularForm.controls['newscolumns'].setValue('');
          }
          this.memLoyVal = true;
          this.valueName = data['tiles'][0]['tableName'];
          this.chartService.getParticularColumns(data['tiles'][0]['tableName']).then(data => {
            if(data['status'] == 'success'){
              this.columns = data['columns'];
              //console.log(this.columns);
            }
          });
          this.regularForm.controls['columns'].setValue(data['tiles'][0]['mainRecordId']);
          this.regularForm.controls['newscolumns'].setValue(data['tiles'][0]['columnName']);
        }
      }
    });
  }

  selecIcon(iconVal){
    this.selectedIcon = iconVal;
  }

  selectColumn(tableVal, event){
    if(tableVal == 'users'){
      this.chartService.getColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
      this.memLoyVal = false;
      this.newsletter = false;
      this.newsletterSelectedColumn = '';
      this.regularForm.controls['newscolumns'].setValue('');
    }
    else{
      if(tableVal == 'newsletter'){
        this.newsletter = true;
      }
      else{
        this.newsletter = false;
        this.newsletterSelectedColumn = '';
        this.regularForm.controls['newscolumns'].setValue('');
      }
      this.memLoyVal = true;
      this.valueName = event.target.options[event.target.options.selectedIndex].text;
      this.chartService.getParticularColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
    }
  }

  getstatscolumns(event){
    this.chartService.getnewsletterColumns(new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.newsletterColumns = data['columns'];
        //console.log(this.columns);
      }
    });
  }

  onReactiveFormSubmit(){
    if(this.tileEdit == 0){
        let tilesData = {
        name: this.regularForm.controls['name'].value,
        value: this.regularForm.controls['value'].value,
        columns: (this.memLoyVal) ? '' : this.regularForm.controls['columns'].value,
        color: this.regularForm.controls['color'].value,
        icon: this.selectedIcon,
        uId: 1,
        subTableName: this.regularForm.controls['newscolumns'].value,
        mainRecordId: (this.memLoyVal) ? this.regularForm.controls['columns'].value : '',
        newsletterColumns: this.newsletterSelectedColumn
      }
      console.log(tilesData);
      this.chartService.generateTiles(tilesData).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('Tile added Successfullly.', '', this.getConfig());
          this.chartService.getTiles(1, new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
              this.tiles = data['tiles'];
              this.tilesCount = data['tiles'].length;
            }
            else{
              this.tiles = [];
            }
            this.tileEdit = 0;
            this.regularForm.reset();
            this.isCollapsed = true;
          });
        }
      });
    }
    else{
      let tilesData = {
        name: this.regularForm.controls['name'].value,
        value: this.regularForm.controls['value'].value,
        columns: (this.memLoyVal) ? '' : this.regularForm.controls['columns'].value,
        color: this.regularForm.controls['color'].value,
        icon: this.selectedIcon,
        uId: 1,
        subTableName: this.regularForm.controls['newscolumns'].value,
        mainRecordId: (this.memLoyVal) ? this.regularForm.controls['columns'].value : '',
        newsletterColumns: this.newsletterSelectedColumn,
        id: this.tileEdit
      }
      console.log(tilesData);
      this.chartService.UpdateTiles(tilesData).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('Updated Successfullly.', '', this.getConfig());
          this.chartService.getTiles(1, new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
              this.tiles = data['tiles'];
              this.tilesCount = data['tiles'].length;
            }
            else{
              this.tiles = [];
            }
            this.tileEdit = 0;
            this.regularForm.reset();
            this.isCollapsed = true;
          });
        }
      });
    }
  }

  cancel(){
    this.tileEdit = 0;
    this.regularForm.reset();
    this.isCollapsed = true;
  }

  deleteTile(id){
    this.chartService.deleteTile(id).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Deleted Successfullly.', '', this.getConfig());
        this.chartService.getTiles(1, new Date().getTime()).then(data => {
          if(data['status'] == 'success'){
            this.tiles = data['tiles'];
            this.tilesCount = data['tiles'].length;
          }
          else{
            this.tiles = [];
          }
          this.tileEdit = 0;
          this.regularForm.reset();
          this.isCollapsed = true;
        });
      }
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}
