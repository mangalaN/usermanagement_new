import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuildChartComponent } from "./build-chart/build-chart.component";
import { TilesConfigComponent } from "./tiles-config/tiles-config.component";
import { SettingsComponent } from "./settings/settings.component";
import { AddExtraFieldsComponent } from "./add-extra-fields/add-extra-fields.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'build-chart',
        component: BuildChartComponent,
        data: {
          title: 'build chart'
        }
      },
      {
        path: 'tiles-config',
        component: TilesConfigComponent,
        data: {
          title: 'tiles config'
        }
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: {
          title: 'Settings'
        }
      },
      {
        path: 'add-fields',
        component: AddExtraFieldsComponent,
        data: {
          title: 'Add Fields'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigurationsRoutingModule { }
