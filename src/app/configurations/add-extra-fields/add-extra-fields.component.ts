import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { AddFieldsService } from '../../shared/data/addFields.service';



@Component({
  selector: 'app-add-extra-fields',
  templateUrl: './add-extra-fields.component.html',
  styleUrls: ['./add-extra-fields.component.scss']
})
export class AddExtraFieldsComponent implements OnInit {

	columns = [
		{ name: 'Field Name' },
		{ name: 'Field Type' },
		{ name: 'Mandatory' },
		{ name: 'Validation' },
		{ name: 'Action' }
	];

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;

	addFieldsDetails = [];
	showForm = false;
	showValidationMsg = false;
	fieldId = 0;
	edit = false;
	fieldForm: FormGroup;
	// fields = {
	// 	'label':'',
	// 	'datatype':'',
	// 	'length':'',
	// 	'mandatory':false,
	// 	'validation':false,
	// 	'validationMsg':'',
	// 	'id':''
	// }


	constructor(private snotifyService: SnotifyService, public addFieldsService: AddFieldsService) { }

	ngOnInit() {
		this.fieldForm = new FormGroup({
            'label': new FormControl(null, [Validators.required]),
            'datatype': new FormControl(null, [Validators.required]),
            'length': new FormControl(null, [Validators.required]),
            'mandatory': new FormControl(false, null),
            'validation': new FormControl(false, null),
            'validationMsg': new FormControl(null, null)
        });
		// pass organistaion id and admin id
		this.addFieldsService.getFields(new Date().getTime(), 1, 1).then(data => {
			if(data['status'] = 'success'){
				//console.log(data);
				let datas = [];
				for(let i = 0; i < data['fields'].length; i++){
					let encodeJSON = JSON.parse(data['fields'][i]['fields']);
					let fields = {
						'label': encodeJSON['label'],
						'datatype': encodeJSON['datatype'],
						'length': encodeJSON['length'],
						'mandatory': encodeJSON['mandatory'],
						'validation': encodeJSON['validation'],
						'validationMsg': encodeJSON['validationMsg'],
						'id': data['fields'][i]['id']
					}
					datas.push(fields);
				}
				this.addFieldsDetails = datas;
				console.log(this.addFieldsDetails);
			}
		});
	}

	showMsg(val){
		//alert(val);
		this.showValidationMsg = val;
	}

	memToggle(){
		this.showForm = true;
	}

	cancel(){
		this.showForm = false;
		this.fieldForm.reset();
	}

	editFields(id){
		this.fieldId = id;
		this.edit = true;
		this.addFieldsService.getFieldsById(this.fieldId, new Date().getTime(), 1, 1).then(data => {
			if(data['status'] = 'success'){
				this.showForm = true;
				//console.log(data['fields']);
				this.fieldForm.controls['label'].setValue(data['fields']['label']);
				this.fieldForm.controls['datatype'].setValue(data['fields']['datatype']);
				this.fieldForm.controls['length'].setValue(data['fields']['length']);
				this.fieldForm.controls['mandatory'].setValue(data['fields']['mandatory']);
				this.fieldForm.controls['validation'].setValue(data['fields']['validation']);
				this.fieldForm.controls['validationMsg'].setValue(data['fields']['validationMsg']);
			}
		});
	}

	updateFields(){
		let fields = {
			'label':this.fieldForm.controls['label'].value,
			'datatype':this.fieldForm.controls['datatype'].value,
			'length':this.fieldForm.controls['length'].value,
			'mandatory':this.fieldForm.controls['mandatory'].value,
			'validation':this.fieldForm.controls['validation'].value,
			'validationMsg':this.fieldForm.controls['validationMsg'].value,
			'id': this.fieldId
		};
		this.addFieldsService.editFields(fields, new Date().getTime(), 1, 1).then(data => {
			if(data['status'] = 'success'){
				this.addFieldsDetails = data['fields'];
				this.edit = false;
				this.snotifyService.success('Field Details Were Added Successfullly', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime(), 1, 1).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						this.showForm = false;
						let datas = [];
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

	deleteFields(id){
		this.addFieldsService.deleteFields(id, new Date().getTime(), 1, 1).then(data => {
			if(data['status'] = 'success'){
				this.snotifyService.success('Deleted Successfullly', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime(), 1, 1).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						this.showForm = false;
						let datas = [];
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

	addFields(){
		let fields = {
			'label':this.fieldForm.controls['label'].value,
			'datatype':this.fieldForm.controls['datatype'].value,
			'length':this.fieldForm.controls['length'].value,
			'mandatory':this.fieldForm.controls['mandatory'].value,
			'validation':this.fieldForm.controls['validation'].value,
			'validationMsg':this.fieldForm.controls['validationMsg'].value
		};
		this.addFieldsService.addFields(fields, new Date().getTime(), 1, 1).then(data => {
			if(data['status'] = 'success'){
				this.snotifyService.success('Field Details Were Added Successfullly', '', this.getConfig());
				this.addFieldsService.getFields(new Date().getTime(), 1, 1).then(data => {
					if(data['status'] = 'success'){
						//console.log(data['fields']);
						let datas = [];
						this.showForm = false;
						for(let i = 0; i < data['fields'].length; i++){
							let encodeJSON = JSON.parse(data['fields'][i]['fields']);
							let fields = {
								'label': encodeJSON['label'],
								'datatype': encodeJSON['datatype'],
								'length': encodeJSON['length'],
								'mandatory': encodeJSON['mandatory'],
								'validation': encodeJSON['validation'],
								'validationMsg': encodeJSON['validationMsg'],
								'id': data['fields'][i]['id']
							}
							datas.push(fields);
						}
						this.addFieldsDetails = datas;
					}
				});
			}
		});
	}

	getConfig(): SnotifyToastConfig {
		this.snotifyService.setDefaults({
		  global: {
		      newOnTop: this.newTop,
		      maxAtPosition: this.blockMax,
		      maxOnScreen: this.dockMax,
		  }
		});
		return {
		  bodyMaxLength: this.bodyMaxLength,
		  titleMaxLength: this.titleMaxLength,
		  backdrop: this.backdrop,
		  position: this.position,
		  timeout: this.timeout,
		  showProgressBar: this.progressBar,
		  closeOnClick: this.closeClick,
		  pauseOnHover: this.pauseHover
		};
    }
}
