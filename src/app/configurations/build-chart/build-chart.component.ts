import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { ChartCongigService } from '../../shared/data/chart-congig.service';


@Component({
  selector: 'app-build-chart',
  templateUrl: './build-chart.component.html',
  styleUrls: ['./build-chart.component.scss']
})
export class BuildChartComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  regularForm: FormGroup;
  columns;
  isCollapsed = true;


  loadingIndicator: boolean = true;
  reorderable: boolean = true;

  tblcolumns = [
      { prop: 'ChartName' },
      { prop: 'ChartType' },
      // { name: 'Preview' },
      { name: 'Action' }
  ];

  valueName;
  newsletter = false;
  newsletterSelectedColumn = '';
  userData = true;
  between = false;
  memLoyVal = false;
  newsletterColumns = '';
  chartsCount;
  charts;
  chartId = 0;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(private snotifyService: SnotifyService, public chartService: ChartCongigService) {}

  ngOnInit() {
    this.regularForm = new FormGroup({
      'name': new FormControl(null, null),
      'chartType': new FormControl(null, null),
      'data': new FormControl(null, null),
      'xaxsis': new FormControl(null, null),
      'yaxsis': new FormControl(null, null),
      'columns': new FormControl(null, null),
      'daterange': new FormControl(null, null),
      'newscolumns': new FormControl(null, null),
      'xdata': new FormControl(null, null)
    }, {updateOn: 'blur'});
    this.chartService.getCharts(1, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.charts = data['charts'];
        this.chartsCount = data['charts'].length;
        setTimeout(() => { this.loadingIndicator = false; }, 1500);
      }
      else{
        this.charts = [];
        this.chartsCount = 0;
      }
    });
  }

  selectColumn(tableVal, event){
    // if(val == 'user'){
    //   this.memLoyVal = false;
    //   this.newsletter = false;
    //   this.newsletterSelectedColumn = '';
    //   this.regularForm.controls['newscolumns'].setValue('');
    // }
    // else{
    //   this.memLoyVal = true;
    // }
    this.valueName = event.target.options[event.target.options.selectedIndex].text;
    if(tableVal == 'users'){
      this.chartService.getColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
      this.userData = true;
      this.memLoyVal = false;
      this.newsletter = false;
      this.newsletterSelectedColumn = '';
      this.regularForm.controls['newscolumns'].setValue('');
    }
    else{
      this.userData = false;
      if(tableVal == 'newsletter'){
        this.newsletter = true;
      }
      else{
        this.newsletter = false;
        this.newsletterSelectedColumn = '';
        this.regularForm.controls['newscolumns'].setValue('');
      }
      this.memLoyVal = true;
      this.chartService.getParticularColumns(tableVal).then(data => {
        if(data['status'] == 'success'){
          this.columns = data['columns'];
          //console.log(this.columns);
        }
      });
    }
  }

  getstatscolumns(event){
    this.chartService.getnewsletterColumns(new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.newsletterColumns = data['columns'];
        //console.log(this.columns);
      }
    });
  }

  onReactiveFormSubmit(){
    if(this.chartId == 0){
      let chartDetail = {
        uid: 1, //login userId
        name: this.regularForm.controls['name'].value,
        chartType: this.regularForm.controls['chartType'].value,
        data: this.regularForm.controls['data'].value,
        xaxsis: this.regularForm.controls['xaxsis'].value,
        yaxsis: this.regularForm.controls['yaxsis'].value,
        columns: (this.userData) ? this.regularForm.controls['columns'].value : '',
        daterange: (this.userData) ? this.regularForm.controls['daterange'].value : '',
        newsletterColumns: this.newsletterSelectedColumn,
        xdata: (this.userData) ? this.regularForm.controls['xdata'].value : '',
        subTableName: this.regularForm.controls['newscolumns'].value,
        mainRecordId: (!this.userData) ? this.regularForm.controls['columns'].value : ''
      }
      console.log(chartDetail);
      this.chartService.generateUserChart(chartDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('Chart addition was Successfullly.', '', this.getConfig());
          this.regularForm.reset();
          this.isCollapsed = true;
          this.chartService.getCharts(1, new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
              this.charts = data['charts'];
              this.chartsCount = data['charts'].length;
              setTimeout(() => { this.loadingIndicator = false; }, 1500);
            }
          });
        }
      });
    }
    else{
      let chartDetail = {
        uid: 1, //login userId
        name: this.regularForm.controls['name'].value,
        chartType: this.regularForm.controls['chartType'].value,
        data: this.regularForm.controls['data'].value,
        xaxsis: this.regularForm.controls['xaxsis'].value,
        yaxsis: this.regularForm.controls['yaxsis'].value,
        columns: (this.userData) ? this.regularForm.controls['columns'].value : '',
        daterange: (this.userData) ? this.regularForm.controls['daterange'].value : '',
        newsletterColumns: this.newsletterSelectedColumn,
        xdata: (this.userData) ? this.regularForm.controls['xdata'].value : '',
        subTableName: this.regularForm.controls['newscolumns'].value,
        mainRecordId: (!this.userData) ? this.regularForm.controls['columns'].value : '',
        id: this.chartId
      }
      console.log(chartDetail);
      this.chartService.UpdateChart(chartDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('Updated Successfullly.', '', this.getConfig());
          this.regularForm.reset();
          this.isCollapsed = true;
          this.chartId = 0;
          this.chartService.getCharts(1, new Date().getTime()).then(data => {
            if(data['status'] == 'success'){
              this.charts = data['charts'];
              this.chartsCount = data['charts'].length;
              setTimeout(() => { this.loadingIndicator = false; }, 1500);
            }
            else{
              this.charts = [];
              this.chartsCount = 0;
            }
          });
        }
      });
    }
  }

  cancelEdit(){
    this.isCollapsed = true;
    this.regularForm.reset();
    this.chartId = 0;
  }

  deleteChart(id){
    this.chartService.deleteChart(id).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('Chart Deleted Successfullly', '', this.getConfig());
        this.chartService.getCharts(1, new Date().getTime()).then(data => {
          if(data['status'] == 'success'){
            this.charts = data['charts'];
            this.chartsCount = data['charts'].length;
            setTimeout(() => { this.loadingIndicator = false; }, 1500);
          }
          else{
            this.charts = [];
            this.chartsCount = 0;
          }
        });
      }
    });
  }

  editChart(id){
    this.chartId = id;
    this.isCollapsed = false;
    this.chartService.getChartsById(new Date().getTime(), id).then(data => {
      if(data['status'] == 'success'){
        console.log(data);
        this.regularForm.controls['name'].setValue(data['chart'][0]['Name']);
        this.regularForm.controls['chartType'].setValue(data['chart'][0]['chartType']);
        this.regularForm.controls['data'].setValue(data['chart'][0]['dataTable']);

        if(data['chart'][0]['dataTable'] == 'users'){
          this.userData = true;
          this.memLoyVal = false;
          this.newsletter = false;
          this.newsletterSelectedColumn = '';
          this.regularForm.controls['newscolumns'].setValue('');
          this.regularForm.controls['xdata'].setValue(data['chart'][0]['xaxsis']);
          this.regularForm.controls['daterange'].setValue(data['chart'][0]['daterange']);
          this.chartService.getColumns('users').then(userColumn => {
            if(userColumn['status'] == 'success'){
              this.columns = userColumn['columns'];
              this.regularForm.controls['columns'].setValue(data['chart'][0]['columns']);
              //console.log(this.columns);
            }
          });
        }
        else{
          this.userData = false;
          if(data['chart'][0]['dataTable'] == 'newsletter'){
            this.newsletter = true;
            this.getstatscolumns('newsletter');
          }
          else{
            this.newsletter = false;
            this.newsletterSelectedColumn = '';
            this.regularForm.controls['newscolumns'].setValue('');
          }
          this.memLoyVal = true;
          this.chartService.getParticularColumns(data['chart'][0]['dataTable']).then(otherColumns => {
            if(otherColumns['status'] == 'success'){
              this.columns = otherColumns['columns'];
              this.newsletterSelectedColumn = data['chart'][0]['mainRecordId'];
              this.regularForm.controls['columns'].setValue(data['chart'][0]['mainRecordId']);
              this.regularForm.controls['newscolumns'].setValue(data['chart'][0]['subTableName']);
              // for(let i = 0; i < this.newsletterColumns.length; i++){
              //   if(this.newsletterColumns[i]['title'] == data['chart'][0]['columns']){
              //     this.regularForm.controls['newscolumns'].setValue(this.newsletterColumns[i]['id']);
              //   }
              // }
              //console.log(this.columns);
            }
          });
        }
        //this.regularForm.controls['newscolumns'].setValue(data['chart'][0]['Name']);
        this.regularForm.controls['xaxsis'].setValue(data['chart'][0]['xvalues']);
        this.regularForm.controls['yaxsis'].setValue(data['chart'][0]['yvalues']);
        //
        //
      }
    });
  }

  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}
