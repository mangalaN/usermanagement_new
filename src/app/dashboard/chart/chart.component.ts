import { Component, OnInit } from '@angular/core';

import * as chartsData from '../../shared/configs/ngx-charts.config';
import * as shape from 'd3-shape';

import { ChartCongigService } from '../../shared/data/chart-congig.service';
import { OrganizationService } from '../../shared/data/organization.service';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {

  lineChartXAxisLabel;
  lineChartYAxisLabel;
  lineChartName;
  ChartColorScheme = {
      domain: ['#666EE8', '#FF9149', '#FF4961', '#AAAAAA']
  };
  lineChartMulti;

  barChartXAxisLabel;
  barChartYAxisLabel;
  barChartName;
  barChartSingle = [];
  barChartColorScheme = {
      domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  pieChartColorScheme = {
      domain: ['#666EE8', '#28D094', '#FF4961', '#AAAAAA']
  };
  pieChartSingle = [
    {
      "name": "Germany",
      "value": 894
    },
    {
      "name": "USA",
      "value": 500
    },
    {
      "name": "France",
      "value": 720
    }
  ];

  tiles;
  charts;
  public isSuperAdmin: boolean = false;
  public widgets;
  chartsForAdmin;

  constructor(public chartService: ChartCongigService, public organizationService: OrganizationService) {
    // this.lineChartMulti = [
    //   {
    //     "name": "Users Count",
    //     "series": []
    //   }
    // ];
    // this.barChartSingle = [];
    let users = JSON.parse(localStorage.getItem('currentUser'));
    if(users['role_id'] == '2'){
      this.isSuperAdmin = true;
    }

    this.organizationService.getListCount().then(data => {
      console.log('LIST Count - ' + JSON.stringify(data));
    });

    this.chartService.buildCharts(1).then(data => {
      if(data['status'] == 'success'){
        //console.log(data['buildCharts']);
        let chartData = [];
        for(let i = 0; i < data['buildCharts'].length; i++){
          //console.log(data['buildCharts'][i]['chartData']);
          if(data['buildCharts'][i]['chartData'] != ''){
            if(data['buildCharts'][i]['chartType'] == 'line'){
              let lineChartData = [
                {
                  "name": data['buildCharts'][i]['Name'],
                  "series": []
                }
              ];
              let lineyAxisTicks = [];
              for(let j = 0; j < data['buildCharts'][i]['chartData'].length; j++){
                lineChartData[0].series.push({"name": data['buildCharts'][i]['chartData'][j]['name'], "value": parseInt(data['buildCharts'][i]['chartData'][j]['count'])})
                lineyAxisTicks.push(parseInt(data['buildCharts'][i]['chartData'][j]['count']));
              }
              chartData.push({"layout": [...lineChartData], "yAxisTicks": lineyAxisTicks, "name": data['buildCharts'][i]['Name'], "chartType": data['buildCharts'][i]['chartType'], "xaxis": data['buildCharts'][i]['xvalues'], "yaxis": data['buildCharts'][i]['yvalues']});
              //console.log(chartData);
            }
            let baryAxisTicks = [];
            if(data['buildCharts'][i]['chartType'] == 'bar'){
              let barChartData = [];
              for(let j = 0; j < data['buildCharts'][i]['chartData'].length; j++){
                barChartData.push({"name": data['buildCharts'][i]['chartData'][j]['name'], "value": parseInt(data['buildCharts'][i]['chartData'][j]['count'])})
              }
              chartData.push({"layout": [...barChartData], "yAxisTicks": baryAxisTicks, "name": data['buildCharts'][i]['Name'], "chartType": data['buildCharts'][i]['chartType'], "xaxis": data['buildCharts'][i]['xvalues'], "yaxis": data['buildCharts'][i]['yvalues']});
              //console.log(chartData);
            }
            if(data['buildCharts'][i]['chartType'] == 'pie'){
              let pieChartData = [];
              for(let j = 0; j < data['buildCharts'][i]['chartData'].length; j++){
                pieChartData.push({"name": data['buildCharts'][i]['chartData'][j]['name'], "value": parseInt(data['buildCharts'][i]['chartData'][j]['count'])})
                baryAxisTicks.push(parseInt(data['buildCharts'][i]['chartData'][j]['count']));
              }
              chartData.push({"layout": [...pieChartData], "name": data['buildCharts'][i]['Name'], "chartType": data['buildCharts'][i]['chartType'], "xaxis": data['buildCharts'][i]['xvalues'], "yaxis": data['buildCharts'][i]['yvalues']});
              //console.log(chartData);
            }
          }
        }
        this.charts = chartData;
        console.log(this.charts);
        // this.lineChartXAxisLabel = data['buildCharts']['chartdata'][0]['xvalues'];
        // this.lineChartYAxisLabel = data['buildCharts']['chartdata'][0]['yvalues'];
        // this.lineChartName = data['buildCharts']['chartdata'][0]['Name'];
        // for(let i = 0; i < data['buildCharts']['userplots'].length; i++){
        //   this.lineChartMulti[0].series.push({'name' : data['buildCharts']['userplots'][i]['date'], 'value' : parseInt(data['buildCharts']['userplots'][i]['userCount'])})
        // }
        // this.lineChartMulti = [...this.lineChartMulti];
        // console.log(this.lineChartMulti);
        //
        // // bar chart
        // this.barChartXAxisLabel = data['buildCharts']['chartdata'][1]['xvalues'];
        // this.barChartYAxisLabel = data['buildCharts']['chartdata'][1]['yvalues'];
        // this.barChartName = data['buildCharts']['chartdata'][1]['Name'];
        // for(let i = 0; i < data['buildCharts']['membershipplots'].length; i++){
        //   this.barChartSingle.push({'name' : data['buildCharts']['membershipplots'][i]['plans'], 'value' : parseInt(data['buildCharts']['membershipplots'][i]['usercount'])})
        // }
        // this.barChartSingle = [...this.barChartSingle];
      }
      //console.log(this.barChartSingle);
    });

    this.organizationService.getCharts().then(data => {
      if(data['status'] == 'success'){
        let charts = ['org', 'list', 'subscriber', 'membership', 'email', 'campaign'];
        let chartNames = ['Number of Organizations','Number of Lists','Number of Subscribers','Number of Memberships','Number of Emails Sent','Number of Campaign'];
        let chartData = [];
        for(let j = 0; j < charts.length; j++){
          let graph = [];
          let yAxisTicks = [];
          for(let i = 0; i < data['charts'][charts[j]].length; i++){
            graph.push({"name": data['charts'][charts[j]][i]['date'], "value": parseInt(data['charts'][charts[j]][i]['count'])});
            yAxisTicks.push(parseInt(data['charts'][charts[j]][i]['count']));
          }
          chartData.push({"layout": [...graph], "yAxisTicks": yAxisTicks, "name": chartNames[j], "chartType": 'bar', "xaxis": 'Dates', "yaxis": 'Counts'});
        }
        this.chartsForAdmin = chartData;
      }
    });
  }

  ngOnInit() {
    this.chartService.getTiles(1, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.tiles = data['tiles'];
      }
    });
    this.organizationService.getTiles().then(data => {
      if(data['status'] == 'success'){
        //console.log('Count of tiles - ' + JSON.stringify(data));
        let tiles = ['list', 'subscriber', 'membership', 'email_sent', 'campaign'];
        let tileNames = ['Lists','Subscribers','Memberships','Emails Sent','Campaign'];
        let chartData = [];
        for(let j = 0; j < tiles.length; j++){
          let tileData = [];
          for(let i = 0; i < data['tiles'][tiles[j]].length; i++){
            chartData.push({"name": tileNames[j], "icon": data['tiles'][tiles[j]][i]['icon'], "color": data['tiles'][tiles[j]][i]['color'], "count": parseInt(data['tiles'][tiles[j]][i]['count'])});
          }
          console.log('chartData - ' + JSON.stringify(chartData));
          this.widgets = chartData;
        }
      }
    });
  }

}
