import { Component, OnInit } from '@angular/core';
import {UserService} from '../shared/data/user.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
groups;

  constructor(public userData: UserService) { }

  ngOnInit() {
  	this.userData.getUsers().then(data => {
        console.log(data['user']);
        this.groups = data['user'];
    },
    error => {
    });
  }

}
