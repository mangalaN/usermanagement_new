import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyDisplayComponent } from './loyalty-display.component';

describe('LoyaltyDisplayComponent', () => {
  let component: LoyaltyDisplayComponent;
  let fixture: ComponentFixture<LoyaltyDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyaltyDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyaltyDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
