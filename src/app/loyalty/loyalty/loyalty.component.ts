import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { loyaltyService } from '../../shared/data/loyalty.service';

@Component({
  selector: 'app-loyalty',
  templateUrl: './loyalty.component.html',
  styleUrls: ['./loyalty.component.scss']
})
export class LoyaltyComponent implements OnInit {
	loyaltyform: FormGroup;
	public loyaltydata: any;
	submitted =  false;

	constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, public router: Router, public LoyaltyService: loyaltyService) { }

	ngOnInit() {
		this.loyaltyform = this.formBuilder.group({
			name: ['', Validators.required],
			points: ['', Validators.required],
			expiryDate: ['', Validators.required]
        });
	}

	get f(){ return this.loyaltyform.controls; }

	addloyalty(){
	    this.loyaltydata = false;
	    this.loyaltydata = true;
	}

	cancel(){
  		this.router.navigate(['/loyalty/loyalty-points']);
  	}

	onloyalty() {
		this.submitted = true;
		if (this.loyaltyform.invalid) {
			return;
		}
		this.loyaltydata = {
			name: this.f.name.value,
			points: this.f.points.value,
			expiryDate: this.f.expiryDate.value
		}
		this.LoyaltyService.addloyalty(this.loyaltydata).then(data => {
			if(data['status'] == "success"){
				this.router.navigate(['/loyalty/loyalty-points']);
			}
			else if(data['status'] == "Failed"){
			}
			console.log(data);
		},
		error => {
		});
	}
}
