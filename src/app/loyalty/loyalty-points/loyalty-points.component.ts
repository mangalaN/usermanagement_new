import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { loyaltyService } from '../../shared/data/loyalty.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

@Component({
  selector: 'app-loyalty-points',
  templateUrl: './loyalty-points.component.html',
  styleUrls: ['./loyalty-points.component.scss']
})
export class LoyaltyPointsComponent implements OnInit {
	public loyaltydisplay: any;
	rows = [];
    temp = [];

    // Table Column Titles
    columns = [
        { name: 'Name' },
        { name: 'Points' },
        {name: 'expiryDate' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

	constructor(public loyaltyservice: loyaltyService, public router: Router) { }

	ngOnInit() {
		this.loyaltyservice.getLoyalty().then(data => {
	        console.log(data['loyalty']);
	        this.loyaltydisplay = data['loyalty'];
	    },
	    error => {
	    });
	}

	editloyalty(id){
		this.router.navigate(['/edit-loyalty'],{queryParams:{id:id}});
	}

	deleteloyalty(id){
		this.loyaltyservice.deleteloyalty(id).then(data => {
	        window.location.reload();
	    },
	    error => {
	    });
	}

}
