import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LoyaltyRoutingModule } from "./loyalty-routing.module";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LoyaltyComponent } from "./loyalty/loyalty.component";
import { LoyaltyPointsComponent } from "./loyalty-points/loyalty-points.component";
import { LoyaltyDisplayComponent } from "./loyalty-display/loyalty-display.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';

@NgModule({
    imports: [
        CommonModule,
        LoyaltyRoutingModule,
        NgxDatatableModule,
        Ng2SmartTableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot()
    ],
    declarations: [
        LoyaltyComponent,
        LoyaltyPointsComponent,
        LoyaltyDisplayComponent
    ]
})
export class LoyaltyModule { }
