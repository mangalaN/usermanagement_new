import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoyaltyPointsComponent } from "./loyalty-points/loyalty-points.component";
import { LoyaltyComponent } from "./loyalty/loyalty.component";
import { LoyaltyDisplayComponent } from "./loyalty-display/loyalty-display.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'loyalty-points',
        component: LoyaltyPointsComponent,
        data: {
          title: 'Loyalty Points'
        }
      },
      {
        path: 'loyalty',
        component: LoyaltyComponent,
        data: {
          title: 'Loyalty'
        }
      },
      {
        path: 'loyalty-display',
        component: LoyaltyDisplayComponent,
        data: {
          title: 'Loyalty Display'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoyaltyRoutingModule { }
