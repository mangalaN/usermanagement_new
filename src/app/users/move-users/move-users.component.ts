import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { UserService } from '../../shared/data/user.service';
import { ListsService } from '../../shared/data/lists.service';

@Component({
  selector: 'app-move-users',
  templateUrl: './move-users.component.html',
  styleUrls: ['./move-users.component.scss']
})
export class MoveUsersComponent implements OnInit {

  constructor(public userservice: UserService, private listsservice: ListsService, private router: Router, private route: ActivatedRoute) { }

  listDetails;
  moveFrom;
  moveTo;
  selectedToList = '';
  selectedFromList = '';
  users;
  fromUsers;
  moveSelectedUser = [];
  submitted = false;

  ngOnInit() {
    this.listsservice.getlist().then(data => {
        console.log(data['lists']);
        this.listDetails = data['lists'];
    },
    error => {
    });
    // this.userservice.getUsersSubscribtion().then(data => {
    //     console.log(data['users']);
    //     this.users = data['users'];
    // },
    // error => {
    // });
  }

  removeToList(value, event){
    this.selectedFromList = value;
    console.log(this.selectedFromList);
    this.moveFrom = event.target.options[event.target.options.selectedIndex].text;
    if(value != ''){
      this.userservice.getUsersSubscriptionList(value, new Date().getTime()).then(data => {
          console.log(data['users']);
          this.users = data['users'];
      });
    }
    else{
      this.users = [];
      // this.userservice.getUsersSubscribtion().then(data => {
      //     console.log(data['users']);
      //     this.users = data['users'];
      // },
      // error => {
      // });
    }
  }

  removeFromList(value, event){
    this.selectedToList = value;
    this.moveTo = event.target.options[event.target.options.selectedIndex].text;
    if(value != ''){
      this.userservice.getUsersSubscriptionList(value, new Date().getTime()).then(data => {
          console.log(data['users']);
          this.fromUsers = data['users'];
      });
    }
    else{
      this.fromUsers = [];
      // this.userservice.getUsersSubscribtion().then(data => {
      //     console.log(data['users']);
      //     this.fromUsers = data['users'];
      // },
      // error => {
      // });
    }
  }

  moveUser(){
    this.submitted = true;
    if(this.moveSelectedUser != []){
      this.userservice.moveSelectedUser(this.selectedToList, this.moveSelectedUser).then(data => {
          if(data['status'] == 'success'){
              window.location.reload();
          }
      },
      error => {
      });
    }
    else{
      this.userservice.moveUser(this.selectedToList, this.selectedFromList).then(data => {
          if(data['status'] == 'success'){
              window.location.reload();
          }
      },
      error => {
      });
    }
  }

  chooseUser(uid,checked){
    //console.log(event);
    if(!checked){
      let arr = this.moveSelectedUser;
      for( var i = 0; i < this.moveSelectedUser.length; i++){
         if ( this.moveSelectedUser[i] == uid) {
           arr.splice(i, 1);
         }
      }
    }
    else{
      this.moveSelectedUser.push(uid);
    }
    //console.log(arr);
    console.log(this.moveSelectedUser);
  }

  cancelReset(){
    window.location.reload();
  }
}
