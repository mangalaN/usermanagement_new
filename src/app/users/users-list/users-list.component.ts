import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { UserService } from '../../shared/data/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  dataSource = [];
  p: number = 1;
  pageSize: number = 5;
  style = 'material';
  title = 'Snotify title!';
  body = 'Lorem ipsum dolor sit amet!';
  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;
  collectionSize;

  constructor(private snotifyService: SnotifyService, public userservice: UserService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
      this.userservice.getUsers().then(data => {
      console.log(data['user']);
      if(data['status'] == 'success'){
        this.dataSource = data['user'];
        this.collectionSize = data['user'].length;
      }
    },
    error => {
    });
  }

  edituser(id){
    this.router.navigate(['/users/edit-user'],{queryParams:{id:id}});
  }

  deleteuser(id){
    this.userservice.deleteUser(id).then(data => {
      this.userservice.getUsertime(new Date().getTime()).then(data => {
        if(data['status'] == 'success'){
          this.dataSource = data['user'];
          this.collectionSize = data['user'].length;
        }
      });
      this.snotifyService.error('Deleted Successfullly', '', this.getConfig());
    },
    error => {
    });
  }

  getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
  }

}
