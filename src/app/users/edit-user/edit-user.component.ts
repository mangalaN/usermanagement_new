import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { loyaltypointService } from '../../shared/data/loyaltypoint.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

import { UserService } from '../../shared/data/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  regularForm: FormGroup;
  userId;
  gender;
  options = {
    types: ['geocode'],
    componentRestrictions: { country: 'AU' }
  };
  newaddress = '';
  submitted = false;
  user;
  listSubscription = false;
  userImage;

  loyaltyPointsColumns = [
    { name: 'Name' },
    { name: 'Redeemed Points' },
    { name: 'Remaining Points' },
    { name: 'Redeemed On' }
  ];
  loyaltyPointDisplay;

  transactionColumns = [
    { name: 'Transaction ID' },
    { name: 'User ID' },
    { name: 'Membership ID' },
    { name: 'Amount' },
    { name: 'Renewal Date' },
    { name: 'Expiry Date' }
  ];
  listSub;
  loyaltyPointEarned;
  loyaltyPointsEarnedColumns = [
    { name: 'Loyalty' },
    { name: 'Points Earned' },
    { name: 'Transaction ID' }
  ]

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  constructor(private snotifyService: SnotifyService, public loyaltyPointService: loyaltypointService, public userservice: UserService, private router: Router, private route: ActivatedRoute) {
    this.regularForm = new FormGroup({
      'username': new FormControl(null, [Validators.required]),
      'firstName': new FormControl(null, [Validators.required]),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'dob': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
      'lastName': new FormControl(null, [Validators.required]),
      'genderRadios': new FormControl(null, [Validators.required]),
      'address': new FormControl(null, [Validators.required]),
      'phone': new FormControl(null, [Validators.required])
        // 'inputEmail': new FormControl(null, [Validators.required, Validators.email]),
        // 'password': new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(24)]),
        // 'textArea': new FormControl(null, [Validators.required]),
        // 'radioOption': new FormControl('Option one is this')
    }, {updateOn: 'blur'});
    this.route.queryParams.subscribe(params => {this.userId = params['id'];});
    this.userservice.getUser(this.userId, new Date().getTime()).then(data => {
      if(data['status'] == 'success'){
        this.user = data['user'][0];
        this.regularForm.controls['username'].setValue(data['user'][0].username);
        this.regularForm.controls['firstName'].setValue(data['user'][0].firstName);
        this.regularForm.controls['email'].setValue(data['user'][0].email);
        this.regularForm.controls['dob'].setValue(data['user'][0].dob);
        this.regularForm.controls['password'].setValue(data['user'][0].password);
        this.regularForm.controls['lastName'].setValue(data['user'][0].lastName);
        this.regularForm.controls['address'].setValue(data['user'][0].address);
        if(data['user'][0].gender == 'Female'){
          this.regularForm.controls['genderRadios'].setValue('Female');
        }
        else{
          this.regularForm.controls['genderRadios'].setValue('Male');
        }

        this.regularForm.controls['phone'].setValue(data['user'][0].phone);
        this.gender = data['user'][0].gender;
        if(this.user.listName != ''){
          this.listSubscription = true;
        }
        this.userImage = data['user'][0].profileImage;
      }
    });
  }

  ngOnInit(){
    this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
      if(data['status'] == 'success'){
        console.log(data);
        this.listSub = data['rows'];
      }
    });
    this.loyaltyPointService.getUserLoyaltyPoints(this.userId).then(data=>{
      if(data['status'] == 'success'){
        this.loyaltyPointDisplay = data['loyalPoints'];
      }
    });
    this.loyaltyPointEarned = [];
  }

  onReactiveFormSubmit(){
    this.submitted = true;
    let editAddress = '';
    if(this.newaddress == '' ){
      editAddress = this.regularForm.controls['address'].value;
    }
    else{
      editAddress = this.newaddress;
    }
    console.log(this.regularForm.controls['genderRadios'].value);
    let userDetail = {
      username: this.regularForm.controls['username'].value,
      password: this.regularForm.controls['password'].value,
      firstName: this.regularForm.controls['firstName'].value,
      lastName: this.regularForm.controls['lastName'].value,
      email: this.regularForm.controls['email'].value,
      dob: this.regularForm.controls['dob'].value,
      genderRadios: this.regularForm.controls['genderRadios'].value,
      address: editAddress,
      phone: this.regularForm.controls['phone'].value,
      profileImage: this.user['profileImage'].split('/').pop().split('#')[0].split('?')[0],
      id: this.userId,
      organization: 1
    }
    this.userservice.editUser(userDetail).then(data => {
      if(data['status'] == 'success'){
        this.snotifyService.success('User Details Updates Successfullly', '', this.getConfig());
        this.submitted = false;
        this.userservice.getUser(this.userId, new Date().getTime()).then(data => {
          if(data['status'] == 'success'){
            this.user = data['user'][0];
            this.regularForm.controls['username'].setValue(data['user'][0].username);
            this.regularForm.controls['firstName'].setValue(data['user'][0].firstName);
            this.regularForm.controls['email'].setValue(data['user'][0].email);
            this.regularForm.controls['dob'].setValue(data['user'][0].dob);
            this.regularForm.controls['password'].setValue(data['user'][0].password);
            this.regularForm.controls['lastName'].setValue(data['user'][0].lastName);
            this.regularForm.controls['address'].setValue(data['user'][0].address);
            this.regularForm.controls['genderRadios'].setValue(data['user'][0].gender);
            this.regularForm.controls['phone'].setValue(data['user'][0].phone);
            this.gender = data['user'][0].gender;
            if(this.user.listName != ''){
              this.listSubscription = true;
            }

          }
        });
      }
      else{
        alert('error');
      }
    });
  }

  cancelEdit(){
    this.regularForm.reset();
    this.router.navigate(['/users/users']);
  }

  handleAddressChange(address){
    // Do some stuff
    this.newaddress = address.formatted_address;
  }

  unsubscribe(event, id){
    console.log(event);
    let SubscriptionDetail = {
      id:this.userId
    }
    if(event){
      this.userservice.addSubscriptionUser(SubscriptionDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('User Subscription Was Successfullly', '', this.getConfig());
          this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
            if(data['status'] == 'success'){
              this.listSub = data['rows'];
            }
          });
        }
      });
    }
    else{
      this.userservice.unSubscriptionUser(SubscriptionDetail).then(data => {
        if(data['status'] == 'success'){
          this.snotifyService.success('User Was Successfullly Unsubscribed', '', this.getConfig());
          this.userservice.getUserSubscription(this.userId, new Date().getTime()).then(data=>{
            if(data['status'] == 'success'){
              this.listSub = data['rows'];
            }
          });
        }
      });
    }
  }
  getConfig(): SnotifyToastConfig {
    this.snotifyService.setDefaults({
        global: {
            newOnTop: this.newTop,
            maxAtPosition: this.blockMax,
            maxOnScreen: this.dockMax,
        }
    });
    return {
        bodyMaxLength: this.bodyMaxLength,
        titleMaxLength: this.titleMaxLength,
        backdrop: this.backdrop,
        position: this.position,
        timeout: this.timeout,
        showProgressBar: this.progressBar,
        closeOnClick: this.closeClick,
        pauseOnHover: this.pauseHover
    };
   }
}
