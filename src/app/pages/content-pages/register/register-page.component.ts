import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

import { AuthenticationService } from '../../../shared/data/authentication.service';
import { SubscriptionService } from '../../../shared/data/subscription.service';
import { UserService } from '../../../shared/data/user.service';
import { OrganizationService } from '../../../shared/data/organization.service';

@Component({
    selector: 'app-register-page',
    templateUrl: './register-page.component.html',
    styleUrls: ['./register-page.component.scss']
})

export class RegisterPageComponent {
    //@ViewChild('f') registerForm: NgForm;
    userdata;
    usernameerror = false;
    emailerror = false;
    submitted =  false;
    gender = 'M';
    options = {
		types: ['geocode'],
		componentRestrictions: { country: 'AU' }
    }
    registerForm: FormGroup;
    subscriptionForm:FormGroup;
    subscriptionList;
    newaddress;
    selectedSubcription;
    subscribed: boolean = false;
    date;
    public roles;
    public organizations;

    constructor(private router: Router, public userservice: UserService, public organizationservice: OrganizationService, private formBuilder: FormBuilder, private route: ActivatedRoute, public authenticationService: AuthenticationService, public subscriptionService: SubscriptionService) {
    	this.subscriptionService.getSubscription().then(data => {
            this.subscriptionList = data['subscription'];
            console.log('Get Subscription list - ' + JSON.stringify(this.subscriptionList));
        });
        this.organizationservice.getOrganization(new Date().getTime()).then(data => {
          this.organizations = data['organization'];
        });
        this.userservice.getGroups().then(data => {
          this.roles = data['groups'];
        });
    }

	ngOnInit() {
		this.registerForm = this.formBuilder.group({
			uname: ['', Validators.required],
			inputPass: ['', Validators.required],
			fname: ['', Validators.required],
			lname: ['', Validators.required],
			inputEmail: ['', Validators.compose([Validators.email, Validators.required])],
			phone: ['', Validators.required],
			dob: ['', Validators.required],
			genderRadios: 'F',
			address: ['', Validators.required],
            role: ['', Validators.required],
            organization: ['', Validators.required]
		});
	}

    get f() { return this.registerForm.controls; }
    
    getId(id,type){
        this.subscribed = true;
        this.selectedSubcription = type;
    }

    handleAddressChange(address) {
       // Do some stuff
       this.newaddress = address.formatted_address;
    }

    //  On submit click, reset field value
    onSubmit() {
    	console.log(this.f.genderRadios.value);
    	this.userdata = {
			username: this.f.uname.value,
			password: this.f.inputPass.value,
			firstName: this.f.fname.value,
			lastName: this.f.lname.value,
			email: this.f.inputEmail.value,
			phone: this.f.phone.value,
			dob: this.f.dob.value,
			genderRadios: this.f.genderRadios.value,
			address: this.f.address.value,
            profileImage: '',
			sublist: '',
			role: this.f.role.value, //users groupid
            type: this.selectedSubcription,
            organization: this.f.organization.value
        }
    	this.authenticationService.addregister(this.userdata).then(data => {
            console.log(data);
            if(data['status'] == "success"){
              alert("Register Done Successfully");
              this.router.navigate(['/dashboard/charts']);
              //this.router.navigate(['/pages/login']);
            }
            else if(data['status'] == "Failed"){
              alert("Try Registering after some time");
            }
            else if(data['status'] == "Register Exits"){
                alert("Registration Failed")
				if(data['value'] == "Username Already Exits."){
					this.usernameerror = true;
					this.emailerror = false;
				}
				else{
					this.emailerror = true;
					this.usernameerror = false;
				}
            }
        },
        error => {
        	//this.spinner.hide();
        });
        //this.registerForm.reset();
    }

    onLogin() {
        this.router.navigate(['login'], { relativeTo: this.route.parent });
    }
}