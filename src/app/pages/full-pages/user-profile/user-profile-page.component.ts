import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../shared/data/user.service';

@Component({
    selector: 'app-user-profile-page',
    templateUrl: './user-profile-page.component.html',
    styleUrls: ['./user-profile-page.component.scss']
})

export class UserProfilePageComponent implements OnInit {
    users;
    //Variable Declaration
    currentPage: string = "About"

    constructor(public userservice: UserService){

    }

    ngOnInit() {
        // Horizontal Timeline js for user timeline
        // $.getScript('./assets/js/vertical-timeline.js');
        this.users = JSON.parse(localStorage.getItem('currentUser'));
        console.log(this.users);
    }

    showPage(page: string) {
        this.currentPage = page;
    }
}