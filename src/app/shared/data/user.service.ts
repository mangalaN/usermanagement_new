import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getUsers() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsers.php?action=getUsers&q='+this.timestamp)
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUser(id, timestamp) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+timestamp+'&action=get',{ id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsertime(timestamp) {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsers.php?action=getUsers&q='+timestamp)
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    addUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/addUser.php?q='+this.timestamp, { userDetail: userDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    editUser(userDetail) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/editUser.php?q='+this.timestamp, { userDetail })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    deleteUser(id) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/deleteUser.php?q='+this.timestamp, { id })
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscribtion(timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+timestamp+'&action=all')
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getUsersSubscriptionList(listId, timestamp){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+timestamp+'&action=list&listId='+listId)
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    getGroups(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getGroups.php?q='+this.timestamp)
            .subscribe(users => {
                console.log(users);
                resolve(users);
                }, err => {
               console.log("vbn"+JSON.stringify(err));
            });
        });
    }
    moveUser(ToList, FromList){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+this.timestamp+'&action=moveuser', { ToList, FromList })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getUserDetails(uId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+this.timestamp+'&action=userdetail', { uId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getTimeLine(uId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUserById.php?q='+this.timestamp+'&action=getTimeLine', { uId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    moveSelectedUser(ToList, usersId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getUsersSubscription.php?q='+this.timestamp+'&action=moveselecteduser', { ToList, usersId })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    unSubscriptionUser(SubscriptionDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+this.timestamp+'&action=unsub', { SubscriptionDetail })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    addSubscriptionUser(SubscriptionDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+this.timestamp+'&action=resub', { SubscriptionDetail })
          .subscribe(users => {
              console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
    getUserSubscription(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/addSubscription.php?q='+timestamp+'&action=getall', { id })
          .subscribe(users => {
              //console.log(users);
              resolve(users);
              }, err => {
             console.log("vbn"+JSON.stringify(err));
          });
      });
    }
}
