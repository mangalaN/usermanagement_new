import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChartCongigService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getUserColumn(){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getChartConfig.php?action=&q=getUserColumn'+this.timestamp)
          .subscribe(chartconfig => {
              console.log(chartconfig);
              resolve(chartconfig);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getColumns(tableval){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getColumns&q='+this.timestamp, { tableval })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    generateUserChart(chartDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=generatechart&q='+this.timestamp, { chartDetail })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    UpdateChart(chartDetail){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=updatechart&q='+this.timestamp, { chartDetail })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getCharts(userId, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getCharts&q='+timestamp, { userId })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getChartsById(time, id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getChartsbyid&q='+time, { id })
          .subscribe(chartDetails => {
              //console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    buildCharts(userId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=buildCharts&q='+this.timestamp, { userId })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    generateTiles(tilesData){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=generateTiles&q='+this.timestamp, { tilesData })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    UpdateTiles(tilesData){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=updateTiles&q='+this.timestamp, { tilesData })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getTiles(userId, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getTiles&q='+timestamp, { userId })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getTilesById(id, timestamp){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getTilesById&q='+timestamp, { id })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getParticularColumns(tableval){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getParticularColumns&q='+this.timestamp, { tableval })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getTilesCount(userId){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=getTilesCount&q='+this.timestamp, { userId })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    getnewsletterColumns(timestamp){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getChartConfig.php?action=getnewsletterColumns&q='+timestamp)
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    deleteTile(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=deleteTile&q='+this.timestamp, { id })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }

    deleteChart(id){
      return new Promise(resolve => {
      this.http.post('https://click365.com.au/usermanagement/getChartConfig.php?action=deleteChart&q='+this.timestamp, { id })
          .subscribe(chartDetails => {
              console.log(chartDetails);
              resolve(chartDetails);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
}
//chart-congig.service.ts
