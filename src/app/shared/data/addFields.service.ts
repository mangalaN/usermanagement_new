import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AddFieldsService {
    constructor(private http: HttpClient) {}

    getFields(currentTime, orgId, adminId){
    	let details = {
    		'orgId' : orgId,
			'adminId' : adminId
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=get&q='+currentTime, { details })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getFieldsById(id, currentTime, orgId, adminId){
    	let details = {
    		'id': id,
    		'orgId' : orgId,
			'adminId' : adminId
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=getId&q='+currentTime, { details })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addFields(fieldsVal, currentTime, orgId, adminId){
    	let details = {
    		'fieldsVal': fieldsVal,
    		'orgId' : orgId,
			'adminId' : adminId
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=add&q='+currentTime, { details })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    editFields(fieldsVal, currentTime, orgId, adminId){
    	let details = {
    		'fieldsVal': fieldsVal,
    		'orgId' : orgId,
			'adminId' : adminId
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=edit&q='+currentTime, { details })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deleteFields(fieldsVal, currentTime, orgId, adminId){
    	let details = {
    		'id': fieldsVal,
    		'orgId' : orgId,
			'adminId' : adminId
    	};
    	return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/dynamicFields.php?action=delete&q='+currentTime, { details })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}
