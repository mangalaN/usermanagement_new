import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class newsLetterService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    saveNewletter(newsletter) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/saveNewletter.php?q='+this.timestamp, { newsletter })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewletter() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/getNewletter.php?q='+this.timestamp)
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewletterTimeLine(time){
      return new Promise(resolve => {
      this.http.get('https://click365.com.au/usermanagement/getNewletter.php?q='+time)
          .subscribe(newsletter => {
              console.log(newsletter);
              resolve(newsletter);
              }, err => {
             console.log("vbn"+ JSON.stringify(err));
          });
      });
    }
    sendNewletter(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/sendEmail.php?q='+this.timestamp, { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    sendTestEmail(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/sendTestEmail.php?q='+this.timestamp, { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    editNewsletter(newsletterDetail){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/editNewsletter.php?q='+this.timestamp, { newsletterDetail })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    copyNewsletter(id){
      return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/copyNewsletter.php?q='+this.timestamp, { id })
            .subscribe(newsletter => {
                console.log(newsletter);
                resolve(newsletter);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}
