import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class ListsService {

    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' }
    }

    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getlist() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/list.php?action=get&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getNewsletterList(){
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/list.php?action=getNews&q='+this.timestamp)
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    addlist(lists, action) {
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action='+action+'&q='+this.timestamp, { lists })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    getlistById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action=getbyid&q='+this.timestamp, { id })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
    deletelistById(id){
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/list.php?action=delete&q='+this.timestamp, { id })
            .subscribe(lists => {
                console.log(lists);
                resolve(lists);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}
