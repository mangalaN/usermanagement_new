import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { MembershipService } from '../shared/data/membership.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {
  members = [];
  showMembership = false;
  memDetail;
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  @ViewChild('membershipIconLabel') membershipIconLabel : ElementRef;
  membershipForm: FormGroup;

  p: number = 1;
  totalList;
  previousPage: any;
  pageSize: number = 5;
  collectionSize;
  editMem = false;
  memId = 0;
  selectval = '';
  memLogo = '';
  memShowLogo = false;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private http: HttpClient, private snotifyService: SnotifyService, public membershipservice: MembershipService, public router: Router) { }

  ngOnInit() {
    this.membershipForm = new FormGroup({
        'membershipName': new FormControl(null, [Validators.required]),
        'textArea': new FormControl(null, [Validators.required]),
        'price': new FormControl(null, [Validators.required]),
        'duration': new FormControl(null, [Validators.required]),
        'durationIn': new FormControl(null, [Validators.required]),
        'membershipIcon': new FormControl(null, null)
    }, {updateOn: 'blur'});
    this.membershipservice.getMembership().then(data => {
      if(data['membership']){
        this.members = data['membership'];
        this.collectionSize = data['membership'].length;
        this.totalList = data['membership'].length;
      }
    },
    error => {
    });
  }

   get f() { return this.membershipForm.controls; }

   memToggle(){
     this.showMembership = true;
   }

    cancel(){
      //window.location.reload();
      this.showMembership = false;
      this.membershipForm.reset();
      this.memLogo = '';
      this.memShowLogo = false;
      this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
        if(data['membership']){
          this.members = data['membership'];
          this.collectionSize = data['membership'].length;
          this.totalList = data['membership'].length;
        }
      },
      error => {
      });
    }

    addMembership(){
      this.memDetail = {
        memname: this.f.membershipName.value,
        description: this.f.textArea.value,
        duration: this.f.duration.value,
        price: this.f.price.value,
        durationIn: this.f.durationIn.value,
        membershipIcon: this.memLogo
      }
      if(!this.editMem){
        this.membershipservice.addMembership(this.memDetail).then(data => {
          if(data['status']){
            this.snotifyService.success('Saved Successfullly', '', this.getConfig());
              this.showMembership = false;
              this.membershipForm.reset();
              this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
                if(data['membership']){
                  this.members = data['membership'];
                  this.collectionSize = data['membership'].length;
                  this.totalList = data['membership'].length;
                }
              },
              error => {
              });
              //this.router.navigate(['/membership']);
          }
        });
      }
      else{
        this.memDetail = {
          memname: this.f.membershipName.value,
          description: this.f.textArea.value,
          duration: this.f.duration.value,
          price: this.f.price.value,
          durationIn: this.f.durationIn.value,
          membershipIcon: this.memLogo,
          id: this.memId
        };
        this.membershipservice.updateMembership(this.memDetail).then(data => {
          if(data['status']){
            this.snotifyService.success('Updted Successfullly', '', this.getConfig());
              console.log(data['membership']);
              this.membershipForm.reset();
              this.showMembership = false;
              this.editMem = false;
              this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
                if(data['membership']){
                  this.members = data['membership'];
                  this.collectionSize = data['membership'].length;
                  this.totalList = data['membership'].length;
                }
              },
              error => {
              });
          }
        });
      }
    }

    editmembership(id){
      this.membershipservice.getMembershipById(id).then(data => {
        if(data['status'] == 'success'){
            this.membershipForm.controls['membershipName'].setValue(data['membership'][0]['plans']);
            this.membershipForm.controls['textArea'].setValue(data['membership'][0]['description']);
            this.membershipForm.controls['price'].setValue(data['membership'][0]['price']);
            this.membershipForm.controls['duration'].setValue(data['membership'][0]['terms']);
            this.membershipForm.controls['durationIn'].setValue(data['membership'][0]['duration']);
            this.memLogo = data['membership'][0]['membershipIcon'];
            //this.membershipForm.controls['membershipIcon'].setValue(data['membership'][0]['membershipIcon']);
            //this.membershipIconLabel.nativeElement.innerHTML = data['membership'][0]['membershipIcon'];
            this.selectval = data['membership'][0]['duration'];
            this.memId = data['membership'][0]['id'];
            this.showMembership = true;
            this.editMem = true;
            this.memShowLogo = true;
            //console.log(this.membershipForm.controls['membershipIcon'].value);
            //this.router.navigate(['/membership']);
        }
      });
    }

    deletemembership(id){
      this.membershipservice.deleteMembership(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Deleted Successfullly', '', this.getConfig());
          this.membershipservice.getMembershipTime(new Date().getTime()).then(data => {
            if(data['membership']){
              this.members = data['membership'];
              this.collectionSize = data['membership'].length;
              this.totalList = data['membership'].length;
            }
          },
          error => {
          });
        }
      });
    }

    uploadmembershipIcon(files: File[]){
      this.membershipIconLabel.nativeElement.innerHTML = files[0].name;
      this.memLogo = files[0].name;
      //console.log(this.membershipIconLabel.nativeElement);
      //alert(files[0].name);
      this.uploadAndProgress(files);
    }

    uploadAndProgress(files: File[]){
      console.log(files)
      var formData = new FormData();
      Array.from(files).forEach(f => formData.append('file',f));

      this.http.post('https:/click365.com.au/usermanagement/uploadLogo.php?fld=MembershipLogo', formData, {reportProgress: true, observe: 'events'})
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress) {
            //this.percentDone = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            //this.uploadSuccess = true;
          }
      });
    }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }

}
