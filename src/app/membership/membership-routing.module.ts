import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembershipComponent } from "./membership.component";

const routes: Routes = [
    {
        // path: '',
        // children: [{
        //     path: 'cards',
        //     component: CardsComponent
        // }
        // ]
        path: '',
        component: MembershipComponent,
       data: {
         title: 'Membership'
       },
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MembershipRoutingModule { }