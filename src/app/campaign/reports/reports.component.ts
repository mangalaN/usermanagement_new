import { Component, OnInit, ViewChild } from '@angular/core';
import { StatisticsService } from '../../shared/data/statistics.service';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
	reportCount;
	reportDetails = [];
	columns = [
        { name: 'Status' },
        { name: 'Newsletter Name' },
        { name: 'Newsletter Sent' },
        { name: 'Open Rate' },
        { name: 'Bounce Rate' },
        { name: 'Click Through Rate' },
        { name: 'Last Sent' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(public statisticsservice: StatisticsService, private router: Router, private route: ActivatedRoute) { }

  	ngOnInit() {
  		this.statisticsservice.getreports().then(data => {
	        console.log(data['reports']);
	        if(data['status'] == 'success'){
		        this.reportCount = data['reports'].length;
		        /*for (let i = 0; i < data['reports'].length; i++) {
		        	let array = {
		        		Status : data['reports'][i]['status'],
						NewsletterName : data['reports'][i]['name'],
						NewsletterSent : data['reports'][i]['sent'],
						OpenRate : data['reports'][i]['openRate'],
						BounceRate : data['reports'][i]['bounceRate'],
						ClickThroughrate : data['reports'][i]['linkRate'],
						LastSent : data['reports'][i]['date']
		        	}
		        	this.reportDetails.push(array);
		        	//this.reportDetails = data['reports'];
		        }*/
				this.reportDetails = data['reports'];
		        //this.rows = this.reportDetails;
		    }
	    },
	    error => {
	    });
  }
  
  redirect(id){
	  this.router.navigate(['/statistics'],{queryParams:{id:id}});
  }

}
