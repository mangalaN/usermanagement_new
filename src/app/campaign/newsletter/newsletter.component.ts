import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, AbstractControl, NgForm } from '@angular/forms';
import { newsLetterService } from '../../shared/data/newsletter.service';
import { ListsService } from '../../shared/data/lists.service';
import { EmailTemplateService } from '../../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.scss']
})
export class NewsletterComponent implements OnInit {

  columns = [
    { name: 'Name' },
    { name: 'Subject' },
    { name: 'Status' },
    { name: 'Date' },
    { name: 'Action' }
  ];

  newsletterForm: FormGroup;
  newsletterDetails = [];
  condition = true;
  showTable = true;
  newsLetter= {
    'name': '',
    'subject': '',
    'listid':'Select List',
    'to': '',
    'cc': '',
    'bcc': '',
    'body': '',
    'template': '',
    'id': ''
  };
  collectionSize: number;
  p: number = 1;
  pageSize: number = 5;

  public options: Object = {
      charCounterCount: true,
      // Set the image upload parameter.
      imageUploadParam: 'image_param',

      // Set the image upload URL.
      imageUploadURL: 'https://click365.com.au/usermanagement/images',

      // Additional upload params.
      imageUploadParams: {id: 'my_editor'},

      // Set request type.
      imageUploadMethod: 'POST',

      // Set max image size to 5MB.
      imageMaxSize: 5 * 1024 * 1024,

      // Allow to upload PNG and JPG.
      imageAllowedTypes: ['jpeg', 'jpg', 'png'],
      events:  {
      'froalaEditor.initialized':  function () {
        console.log('initialized');
      },
        'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
          //Your code
          if  (images.length) {
          // Create a File Reader.
          const  reader  =  new  FileReader();
          // Set the reader to insert images when they are loaded.
          reader.onload  =  (ev)  =>  {
          const  result  =  ev.target['result'];
          editor.image.insert(result,  null,  null,  editor.image.get());
          console.log(ev,  editor.image,  ev.target['result'])
          };
          // Read image as base64.
          reader.readAsDataURL(images[0]);
          }
          // Stop default upload chain.
          return  false;
        }
    }
  };
  public listDetails: any;
  defaultTemplate;
  emailTemplates;

  timeout = 3000;
  position: SnotifyPosition = SnotifyPosition.centerTop;
  progressBar = true;
  closeClick = true;
  newTop = true;
  backdrop = -1;
  dockMax = 8;
  blockMax = 6;
  pauseHover = true;
  titleMaxLength = 15;
  bodyMaxLength = 80;

  selectList = '';
  edit = false;
  newsLetterId = 0;

    constructor(private snotifyService: SnotifyService, public newsletterservice: newsLetterService, public listsservice: ListsService, public emailtemplateservice: EmailTemplateService) {
      this.newsLetter.body = this.emailtemplateservice.getDefaultTemplate();
      this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();

      this.newsletterservice.getNewletter().then(data => {
        if(data['status']){
          console.log(data['newsletter']);
          this.newsletterDetails = data['newsletter'];
          this.collectionSize = data['newsletter'].length;
        }
      });
      this.listsservice.getNewsletterList().then(data => {
          console.log(data['lists']);
          this.listDetails = data['lists'];
          this.selectList = data['lists'][0]['id'];
          this.newsLetter.listid = data['lists'][0]['id'];
          if(this.newsLetter.listid == 'Select List'){
            this.condition = false;
          }
          else{
            this.condition = true;
          }
      },
      error => {
      });
      this.emailtemplateservice.getNewsTemp(new Date().getTime()).then(data => {
          console.log(data['emailTemplate']);
          if(data['status'] == 'success'){
            this.emailTemplates = data['emailTemplate'];
            //console.log(this.emailTemplates);
          }
      },
      error => {
      });
    }

    getName(name){
      if (!name) return '';
     if (name.length <= 20) {
          return name;
      }

      return name.substr(0, 20) + '...';
    }

    ngOnInit() {
      this.newsletterForm = new FormGroup({
        'name' : new FormControl(null, [Validators.required]),
        'subject' : new FormControl(null, [Validators.required]),
        'listid' : new FormControl(null, null),
        'to' : new FormControl(null, [this.commaSepEmail]),
        'cc' : new FormControl(null, [this.commaSepEmail]),
        'bcc' : new FormControl(null, [this.commaSepEmail]),
        'template' : new FormControl(null, null),
        'body' : new FormControl(null, null)
      });
    }
    commaSepEmail = (control: AbstractControl): { [key: string]: any } | null => {
      if(control.value != '' && control.value != null){
        const emails = control.value.split(',');
        const forbidden = emails.some(email => Validators.email(new FormControl(email)));
        console.log(forbidden);
        return forbidden ? { 'to': { value: control.value } } : null;
      }
    };
    saveNewsletter(){
      this.newsletterservice.saveNewletter(this.newsLetter).then(data => {
        if(data['status']){
          this.showTable = true;
          this.snotifyService.success('Newsletter Saved Successfullly', '', this.getConfig());
          this.newsLetter.name = '';
          this.newsLetter.subject = '';
          this.newsLetter.listid = '';
          this.newsLetter.to = '';
          this.newsLetter.cc = '';
          this.newsLetter.bcc = '';
          this.newsLetter.body = this.defaultTemplate;
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
            }
          });
          //window.location.reload();
        }
      });
    }
    sendnewsletter(id){
      this.newsletterservice.sendNewletter(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Email Sent Successfullly', '', this.getConfig());
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.showTable = true;
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
            }
          });
          //window.location.reload();
        }
      });
    }
    checkvalue(val){
      console.log(val);
      if(val != "Select List"){
        this.newsLetter.to = '';
        this.condition = true;
      }
      else{
        this.condition = false;
      }
    }
    addnews(){
      this.showTable = false;
      this.newsletterForm.controls['name'].setValue('');
      this.newsletterForm.controls['subject'].setValue('');
      this.newsletterForm.controls['listid'].setValue('');
      this.newsletterForm.controls['to'].setValue('');
      this.newsletterForm.controls['cc'].setValue('');
      this.newsletterForm.controls['bcc'].setValue('');
      this.newsletterForm.controls['template'].setValue('');
      this.newsletterForm.controls['body'].setValue(this.defaultTemplate);
    }
    sendtestmail(){
      this.newsletterservice.sendTestEmail(this.newsLetter).then(data => {
        if(data['status']){
          this.snotifyService.success('Test Email Sent Successfullly', '', this.getConfig());
          this.newsletterDetails = data['newsletter'];
          this.collectionSize = data['newsletter'].length;
          this.newsLetter.body = this.defaultTemplate;
        }
      });
    }
    cancel(){
      this.showTable = true;
      this.edit = false;
      this.newsletterForm.controls['name'].setValue('');
      this.newsletterForm.controls['subject'].setValue('');
      this.newsletterForm.controls['listid'].setValue('');
      this.newsletterForm.controls['to'].setValue('');
      this.newsletterForm.controls['cc'].setValue('');
      this.newsletterForm.controls['bcc'].setValue('');
      this.newsletterForm.controls['template'].setValue('');
      this.newsletterForm.controls['body'].setValue(this.defaultTemplate);
      this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
        if(data['status']){
          this.newsletterDetails = data['newsletter'];
          this.collectionSize = data['newsletter'].length;
          this.newsLetter.body = this.defaultTemplate;
        }
      });
    }
    displayTemplate(value){
      for(let i = 0; i < this.emailTemplates.length; i++){
        if(this.emailTemplates[i]['id'] == value){
          this.emailtemplateservice.getEmailTempById(value).then(data => {
              //console.log(data['emailTemplate']);
              if(data['status'] == 'success'){
                this.newsLetter.body = data['emailTemplate'][0]['body'];
                //console.log(this.emailTemplates);
              }
          },
          error => {
          });
        }
        else{
          this.newsLetter.body =  this.defaultTemplate;
        }
      }
    }

    EditNewsletter(id){
      for(let i = 0; i < this.newsletterDetails.length; i++){
        if(this.newsletterDetails[i]['id'] == id){
          this.newsLetter.name = this.newsletterDetails[i]['name'];
          this.newsLetter.subject = this.newsletterDetails[i]['subject'];
          this.newsLetter.listid = this.newsletterDetails[i]['listid'];
          if(this.newsletterDetails[i]['listid'] == "0"){
            this.selectList = 'Select List';
            this.condition = false;
          }
          else{
            this.condition = true;
            this.selectList = this.newsletterDetails[i]['listid'];
          }
          this.newsLetter.to = this.newsletterDetails[i]['to'];
          this.newsLetter.cc = this.newsletterDetails[i]['cc'];
          this.newsLetter.bcc = this.newsletterDetails[i]['bcc'];
          if(this.newsletterDetails[i]['template'] == "0"){
            this.newsLetter.template  = 'Choose Template';
          }
          else{
            this.newsLetter.template  = this.newsletterDetails[i]['template'];
          }
          //this.newsLetter.template = this.newsletterDetails[i]['template'];
          this.newsLetter.body = this.newsletterDetails[i]['message'];
          this.newsLetterId = id;
          this.showTable = false;
          this.edit = true;
        }
      }
    }
    updateNewsletter(){
      let updateNewsLetter = {
        'name': this.newsLetter.name,
        'subject': this.newsLetter.subject,
        'listid':this.newsLetter.listid,
        'to': this.newsLetter.to,
        'cc': this.newsLetter.cc,
        'bcc': this.newsLetter.bcc,
        'body': this.newsLetter.body,
        'template': this.newsLetter.template,
        'id': this.newsLetterId
      };
        this.newsletterservice.editNewsletter(updateNewsLetter).then(data => {
        if(data['status']){
          this.snotifyService.success('Newsletter Edited Successfullly', '', this.getConfig());
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.showTable = true;
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
            }
          });
        }
      });
    }

    CopyNewsletter(id){
      this.newsletterservice.copyNewsletter(id).then(data => {
        if(data['status']){
          this.snotifyService.success('Newsletter Copied Successfullly', '', this.getConfig());
          this.newsletterservice.getNewletterTimeLine(new Date().getTime()).then(data => {
            if(data['status']){
              this.showTable = true;
              this.newsletterDetails = data['newsletter'];
              this.collectionSize = data['newsletter'].length;
              this.newsLetter.body = this.defaultTemplate;
            }
          });
        }
      });
    }

    getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
    }
}
