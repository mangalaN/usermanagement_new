import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsletterComponent } from "./newsletter/newsletter.component";
import { ReportsComponent } from "./reports/reports.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'newsletter',
        component: NewsletterComponent,
        data: {
          title: 'Newsletter Table'
        }
      },
      {
        path: 'reports',
        component: ReportsComponent,
        data: {
          title: 'Reports Table'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampaignRoutingModule { }
