import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { ListsService } from '../shared/data/lists.service';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.scss']
})
export class ListsComponent implements OnInit {
    listData: any;
    public showTable = true;
    public save: any;
    lists = {
        'id': '',
        'name': '',
        'email': '',
        'phonenum': '',
        'address': '',
        'newsletter': ''
    };
    options = {
        types: ['geocode'],
        componentRestrictions: { country: 'AU' }
    }
    p: number = 1;
    totalList;
    previousPage: any;
    pageSize: number = 5;
    collectionSize; newaddress;
    listForm: FormGroup;
    @ViewChild("placesRef") placesRef : GooglePlaceDirective;

    constructor(public listsservice: ListsService) {
        //this.lists = new Array();
    }

    ngOnInit() {
        this.listForm = new FormGroup({
            'fname': new FormControl(null, [Validators.required]),
            'address': new FormControl(null, [Validators.required]),
            'email': new FormControl(null, [Validators.required]),
            'phone': new FormControl(null, [Validators.required]),
            'newsletter': new FormControl(null, null)
        });
        this.listsservice.getlist().then(data => {
            if(data['status'] == 'success'){
                this.listData = data['lists'];
                this.collectionSize = data['lists'].length;
                this.totalList = data['lists'].length;
            }
        });
    }

    public handleAddressChange(address) {
        // Do some stuff
        this.newaddress = address.formatted_address;
    }

    loadPage(page: number) {
        if (page !== this.previousPage) {
          this.previousPage = page;
          //this.loadData();
        }
    }

    addlist(){
        this.showTable = false;
        this.save = true;
    }

    editlist(id){
        this.listsservice.getlistById(id).then(data => {
            this.lists.id = data['lists'][0].id;
            this.lists.name = data['lists'][0].name;
            this.lists.email = data['lists'][0].email;
            this.lists.phonenum = data['lists'][0].phone;
            this.lists.address = this.newaddress; //data['lists'][0].address;
            this.lists.newsletter = data['lists'][0].newsletter;

            this.save = false;
            this.showTable = false;
        },
        error => {
        });
    }

    cancel(){
        this.showTable = true;
        this.save = true;
        this.lists.id = '';
        this.lists.name = '';
        this.lists.email = '';
        this.lists.phonenum = '';
        this.lists.address = '';
        this.lists.newsletter = '';
	}
    deletelist(id){
        this.listsservice.deletelistById(id).then(data => {
            console.log(data['lists']);
            window.location.reload();
        },
        error => {
        });
    }

    list(){
        if(this.lists.id != ''){
            this.listsservice.addlist(this.lists, 'update').then(data => {
                if(data['status']){
                    this.listsservice.getlist().then(data => {
                        if (data['status'] == 'success') {
                            this.listData = data['lists'];
                            this.collectionSize = data['lists'].length;
                            this.totalList = data['lists'].length;
                        }
                    });
                    window.location.reload();
                }
            });
        }
        else{
            this.listsservice.addlist(this.lists, 'add').then(data => {
                if(data['status']){
                    this.listsservice.getlist().then(data => {
                        if (data['status'] == 'success') {
                            this.listData = data['lists'];
                            this.collectionSize = data['lists'].length;
                            this.totalList = data['lists'].length;
                        }
                    });
                    window.location.reload();
                }
            });
        }
    }
}
