import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist";
import { Router, ActivatedRoute } from '@angular/router';

import { StatisticsService } from '../shared/data/statistics.service';

declare var require: any;

//const data: any = require('../../shared/data/chartist.json');

export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})


export class StatisticsComponent implements OnInit {
  newsletterDate;
  newsletterId;
  newsletterDetail;
  subject;
  list;
  totalSent;
  notSent;
  sent;
  totalSentPerc;
  notSentPerc;
  clickCount;
  openCount;
  chartData;
  chartLabels;
  links;
  domains;

  constructor(public statisticsService: StatisticsService, private route: ActivatedRoute, public router: Router) {
    this.route.queryParams.subscribe(params => {
      this.newsletterId = params['id'];
    });
    this.statisticsService.gettimeline(this.newsletterId).then(data => {
      if(data['status']){
          //console.log(data['reports']);
          let opencount = [];
          let clickcount = [];
          let graphdata = [];
          let graphlabels = [];
          for(let i = 0; i < data['reports']['open'].length; i++){
              opencount.push(data['reports']['open'][i]['opencount']);
              graphlabels.push(data['reports']['open'][i]['hr']);
          }
          opencount.push(1);
          for(let i = 0; i < data['reports']['click'].length; i++){
              clickcount.push(data['reports']['click'][i]['clickcount']);
              graphlabels.push(data['reports']['open'][i]['hr']);
          }
          clickcount.push(6);
          graphdata.push({data:opencount, label:'Opened'},{data:clickcount, label:'Clicked'});
          //console.log(graphdata);
          this.chartData = graphdata;
          console.log(this.chartData);
          console.log(typeof(this.chartData));
          this.chartLabels = graphlabels;
      }
    },
    error => {
    });
  }

  ngOnInit() {
    this.statisticsService.getReportById(this.newsletterId).then(data => {
        if(data['status']){
            //document.getElementById('messageTemplate').innerHTML = JSON.parse(data['reports'][0]['message']);
            this.newsletterDetail = JSON.parse(data['reports'][0]['message']);
            this.newsletterDate = data['reports'][0]['date'];
            this.subject = data['reports'][0]['subject'];
            this.list = data['reports'][0]['list'];
            this.totalSent = data['reports'][0]['totalSent'];
            this.notSent = data['reports'][0]['notSent'];
            this.sent = data['reports'][0]['totalSent'] - data['reports'][0]['notSent'];
            this.totalSentPerc = (((data['reports'][0]['totalSent'] - data['reports'][0]['notSent']) / data['reports'][0]['totalSent']) * 100);
            this.notSentPerc = ((data['reports'][0]['notSent'] / data['reports'][0]['totalSent']) * 100);
            this.clickCount = data['reports'][0]['clickCount'];
            console.log(data['reports']);
        }
    },
    error => {
    });

    this.statisticsService.getclickCounts(this.newsletterId).then(data => {
        if(data['status'] = 'success'){
            this.links = data['reports'];
            this.openCount = data['reports'][0]['openCount'];
            console.log(this.clickCount);
        }
    },
    error => {
    });

    this.statisticsService.getEmailDomain(this.newsletterId).then(data => {
          if(data['status'] = 'success'){
                this.domains = data['reports'];
            }
      },
      error => {
      });
  }

}
