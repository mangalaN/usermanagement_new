import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EmailTemplateRoutingModule } from "./email-template-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { EmailTemplateComponent } from "./email-template.component";
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        EmailTemplateRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        NgxPaginationModule,
        FroalaEditorModule.forRoot(), FroalaViewModule.forRoot(),
        UiSwitchModule
    ],
    declarations: [
        EmailTemplateComponent,
   ]
})
export class EmailTemplateModule { }
