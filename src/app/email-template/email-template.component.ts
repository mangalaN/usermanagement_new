import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmailTemplateService } from '../shared/data/email-template.service';
import { SnotifyService, SnotifyPosition, SnotifyToastConfig } from 'ng-snotify';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit {
	emailTemplateForm: FormGroup;
	defaultTemplate;
	emailTemplates;
	p: number = 1;
	pageSize: number = 5;
	collectionSize;
	showTable = true;
	showEdit = false;
	new = true;
	emailTemDetails: any;
	public options: Object = {
	    charCounterCount: true,
	    // Set the image upload parameter.
	    imageUploadParam: 'image_param',

	    // Set the image upload URL.
	    imageUploadURL: 'https://click365.com.au/usermanagement/images',

	    // Additional upload params.
	    imageUploadParams: {id: 'my_editor'},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png'],
    	events:  {
			'froalaEditor.initialized':  function () {
				console.log('initialized');
			},
  			'froalaEditor.image.beforeUpload':  function  (e,  editor,  images) {
			    //Your code
			    if  (images.length) {
					// Create a File Reader.
					const  reader  =  new  FileReader();
					// Set the reader to insert images when they are loaded.
					reader.onload  =  (ev)  =>  {
					const  result  =  ev.target['result'];
					editor.image.insert(result,  null,  null,  editor.image.get());
					console.log(ev,  editor.image,  ev.target['result'])
					};
					// Read image as base64.
					reader.readAsDataURL(images[0]);
		    	}
		    	// Stop default upload chain.
		    	return  false;
		  	}
		}
	};
	success = false;
	error = false;
	editEmailTemDetails = {
		'id': ''
	};

	timeout = 3000;
	position: SnotifyPosition = SnotifyPosition.centerTop;
	progressBar = true;
	closeClick = true;
	newTop = true;
	backdrop = -1;
	dockMax = 8;
	blockMax = 6;
	pauseHover = true;
	titleMaxLength = 15;
	bodyMaxLength = 80;

	constructor(private snotifyService: SnotifyService, private formBuilder: FormBuilder, public emailtemplateservice: EmailTemplateService, public router: Router) {
		this.defaultTemplate = this.emailtemplateservice.getDefaultTemplate();
	}

	ngOnInit() {
		this.emailTemplateForm = this.formBuilder.group({
          	temname: ['', Validators.required],
			temsub: ['', Validators.required],
			tembody: this.defaultTemplate,
          	template:false
      	});
      	this.emailtemplateservice.getEmailTemp().then(data => {
	        console.log(data['emailTemplate']);
	        if(data['status'] == 'success'){
	        	this.emailTemplates = data['emailTemplate'];
	        	this.collectionSize = data['emailTemplate'].length;
	          }
	    },
	    error => {
	    });
	}

	get f() { return this.emailTemplateForm.controls; }

	addet(){
		this.showTable = false;
		this.new = true;
	}

	cancel(){
	    this.showTable = true;
	    this.new = true;
	    this.showEdit = false;
	    this.emailTemplateForm.controls['temname'].setValue('');
	    this.emailTemplateForm.controls['temsub'].setValue('');
	    this.emailTemplateForm.controls['tembody'].setValue(this.defaultTemplate);
	    this.emailTemplateForm.controls['template'].setValue(false);
	    this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
	      console.log(data['emailTemplate']);
	      if(data['status'] == 'success'){
	        this.emailTemplates = data['emailTemplate'];
	        this.collectionSize = data['emailTemplate'].length;
	      }
	    },
	    error => {
	    });
	}

	editet(id){
	    this.emailtemplateservice.getEmailTempById(id).then(data => {
			console.log(data['emailTemplate']);
			this.emailTemplateForm.controls['temname'].setValue(data['emailTemplate'][0].name);
		    this.emailTemplateForm.controls['temsub'].setValue(data['emailTemplate'][0].subject);
		    this.emailTemplateForm.controls['tembody'].setValue(data['emailTemplate'][0].body);
			this.editEmailTemDetails.id = data['emailTemplate'][0].id;
			// this.editEmailTemDetails.name = data['emailTemplate'][0].name;
			// this.editEmailTemDetails.subject = data['emailTemplate'][0].subject;
			// this.editEmailTemDetails.body = data['emailTemplate'][0].body;
			if(data['emailTemplate'][0].type == "news"){
				this.emailTemplateForm.controls['template'].setValue(true);
			}
			else{
				this.emailTemplateForm.controls['template'].setValue(false);
			}
			this.new = true;
			this.showTable = false;
			this.showEdit = true;
			this.success = false;
			this.error = false;
	    },
	    error => {
	    });
	}

	deleteet(id){
        this.emailtemplateservice.deleteEmailTempById(id).then(data => {
	        if(data['status'] == "success"){
	        	this.showTable = true;
		        this.new = true;
	        	this.error = false;
	            this.success = false;
	            this.snotifyService.success('Deleted Successfullly', '', this.getConfig());
		        this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
			        console.log(data['emailTemplate']);
			        if(data['status'] == 'success'){
			        	this.emailTemplates = data['emailTemplate'];
			        	this.collectionSize = data['emailTemplate'].length;
			         }
		  	    },
		  	    error => {
		  	    });
	        }
	    },
	    error => {
	    });
	}

	addEmailTemplate(){
		this.emailTemDetails = {
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value,
			type: this.f.template.value.toString()
		}
		this.emailtemplateservice.addEmailTemp(this.emailTemDetails).then(data => {
			if(data['status']){
		        this.showTable = true;
		        this.new = true;
	        	this.error = false;
	            this.success = true;
		        this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
			        console.log(data['emailTemplate']);
			        if(data['status'] == 'success'){
			        	this.emailTemplates = data['emailTemplate'];
			        	this.collectionSize = data['emailTemplate'].length;
			         }
		  	    },
		  	    error => {
		  	    });
			}
			else{
		        this.showTable = false;
		        this.new = true;
	        	this.error = true;
	            this.success = false;
			}
		});
	}

	updateEmailTemplate(){
		this.emailTemDetails = {
			id: this.editEmailTemDetails.id,
			name: this.f.temname.value,
			subject: this.f.temsub.value,
			body: this.f.tembody.value,
			type: this.f.template.value.toString()
		}
		this.emailtemplateservice.updateEmailTemp(this.emailTemDetails).then(data => {
			if(data['status']){
				this.showTable = true;
				this.showEdit = false;
				this.new = true;
	        	this.error = false;
	            this.success = false;
	            this.snotifyService.success('Updated Successfullly', '', this.getConfig());
				this.emailtemplateservice.getEmailTempTimeStamp(new Date().getTime()).then(data => {
			        console.log(data['emailTemplate']);
			        if(data['status'] == 'success'){
			        	this.emailTemplates = data['emailTemplate'];
			        	this.collectionSize = data['emailTemplate'].length;
			         }
		  	    },
		  	    error => {
		  	    });
			}
			else{
		        this.showTable = false;
		        this.new = true;
	        	this.error = true;
	            this.success = false;
				this.showEdit = true;
			}
		});
	}

	close(){
		this.success = false;
		this.error = false;
	}


	getConfig(): SnotifyToastConfig {
      this.snotifyService.setDefaults({
          global: {
              newOnTop: this.newTop,
              maxAtPosition: this.blockMax,
              maxOnScreen: this.dockMax,
          }
      });
      return {
          bodyMaxLength: this.bodyMaxLength,
          titleMaxLength: this.titleMaxLength,
          backdrop: this.backdrop,
          position: this.position,
          timeout: this.timeout,
          showProgressBar: this.progressBar,
          closeOnClick: this.closeClick,
          pauseOnHover: this.pauseHover
      };
  	}

}
