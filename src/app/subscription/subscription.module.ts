import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SubscriptionRoutingModule } from "./subscription-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { SubscriptionComponent } from "./subscription.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
    imports: [
        CommonModule,
        SubscriptionRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule,
        FormsModule,
        UiSwitchModule,
        GooglePlaceModule,
        ReactiveFormsModule,
        NgxDatatableModule
    ],
    declarations: [
        SubscriptionComponent,
   ]
})
export class SubscriptionModule { }
